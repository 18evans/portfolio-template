package evans18.portfolio.repository

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import evans18.portfolio.database.UserDao
import evans18.portfolio.model.User
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class UserDataSourceTest {

    private lateinit var userDataSource: UserDataSource
    private lateinit var userDao: UserDao

    @Before
    fun setup() {
        userDao = mock()
        userDataSource = UserDataSource(userDao)
    }

    @Test
    fun test_emptyCache_noDataOnApi_returnsEmptyList() {
        runBlocking {
            whenever(userDao.getAllUsers()).thenReturn(listOf())
            assertTrue(userDataSource.getAll().isEmpty())
        }
    }

    @Test
    fun test_emptyCache_hasDataOnApi_returnsApiData() {
        runBlocking {

            whenever(userDao.getAllUsers()).thenReturn(listOf(testUsers[0]))

            assertEquals(1, userDataSource.getAll().size)
            assertEquals(testUsers[0], userDao.getAllUsers()[0])
        }


    }

    @Test
    fun test_hasCacheData_hasApiData_returnsBothData() {
        runBlocking {
            val dbData = listOf(testUsers[0], testUsers[1])
            whenever(userDao.getAllUsers()).thenReturn(dbData)

            val listUsers = userDataSource.getAll()
            //Both cached & API data delivered
            assertEquals(userDao.getAllUsers().size, listUsers.size)
            //First cache data delivered
            assertEquals(listUsers[0], dbData[0])
            assertEquals(listUsers[1], dbData[1])
        }

    }

    companion object {
        private val testUsers = arrayOf(
            User("test", "test", id = 1),
            User("test", "test", id = 2),
            User("test", "test", id = 3)
        )
    }
    //todo maybe add id
}
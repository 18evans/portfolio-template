package evans18.portfolio.models

import evans18.portfolio.model.User
import org.junit.Assert.*
import org.junit.Test

class UserTest {

    //    val listSkillssss = mock<MutableList<Skill>>()
    private val userName = "name"
    private val userEmail = "test@email.com"
    private val userPhoneNr = "+31 6 2323 2323"
    private val job = "software dev"
    private val user = User(userName, userEmail, userPhoneNr, job) //SUT

    @Test
    fun userShouldHavePropertiesInitialized() {
        assertEquals(userName, user.name)
        assertEquals(userEmail, user.email)
        assertEquals(userPhoneNr, user.phone)
//        listSkillssss.add(mock<Skill>())
//        assertEquals(listSkillssss, user.listSkills)
    }
}
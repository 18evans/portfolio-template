package evans18.portfolio.maps

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import evans18.portfolio.Constants.Owner.ownerLat
import evans18.portfolio.Constants.Owner.ownerLong
import java.io.IOException

class GeocodeClient {

    companion object {
        fun getAddress(context: Context, latLng: LatLng): String {
            val geocoder = Geocoder(context)
            val addresses: List<Address>?
            val address: Address?
            var addressText = ""

            try {
                addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
                if (null != addresses && addresses.isNotEmpty()) {
                    address = addresses[0]
                    for (i in 0 until address.maxAddressLineIndex) {
                        addressText += if (i == 0) address.getAddressLine(i) else "\n" + address.getAddressLine(i)
                    }
                }
            } catch (e: IOException) {
                Log.e(this::class.java.simpleName, e.localizedMessage)
            }
            Log.d(this::class.java.simpleName, addressText)
            return addressText
        }

        fun getLatLngFromName(context: Context, location: String): LatLng {
            if (Geocoder.isPresent()) {
                try {
                    val geocoder = Geocoder(context)
                    //get a random result from between the top 5 results
                    val address = geocoder.getFromLocationName(location, 5)
                        .let { it[(0 until it.size).random()] } // get the found Address Objects

                    if (address.hasLatitude() && address.hasLongitude()) {
                        return LatLng(address.latitude, address.longitude)
                    }
//                    }

                } catch (e: IOException) {
                    Log.e(this::class.java.simpleName, e.localizedMessage)
                }
            }
            return LatLng(ownerLat, ownerLong)
        }


    }
}

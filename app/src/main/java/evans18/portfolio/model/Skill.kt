package evans18.portfolio.model

//todo @Entity(tableName = AppDatabase.TABLE_USERS)
data class Skill(val name: String, val type: SkillType, val experienceLevel: ExperienceLevel) : Comparable<Skill> {
    override fun compareTo(other: Skill): Int =
        experienceLevel.levelStrength.compareTo(other.experienceLevel.levelStrength)

    enum class ExperienceLevel(val levelStrength: Int) {
        JUNIOR(1),
        MEDIOR(2),
        SENIOR(3)
    }

    enum class SkillType {
        TECHNICAL,
        DESIGN,
        METHODOLOGY,
        PRACTICE
    }


}

package evans18.portfolio.daggerentities

import android.app.Application
import dagger.Component
import evans18.portfolio.ProfileActivity
import evans18.portfolio.database.AppDatabase
import evans18.portfolio.database.UserDao
import evans18.portfolio.maps.LocationActivity
import evans18.portfolio.repository.UserDataSource

import javax.inject.Singleton

@Singleton
@Component(dependencies = [], modules = [AppModule::class, DatabaseModule::class])
interface AppComponent {

    fun inject(profileActivity: ProfileActivity)
    fun inject(profileActivity: LocationActivity)

    val userDao: UserDao

    fun database(): AppDatabase

    fun userDataSource(): UserDataSource

    fun application(): Application

}
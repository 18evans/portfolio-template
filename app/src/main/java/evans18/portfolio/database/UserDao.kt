package evans18.portfolio.database

import androidx.room.*
import androidx.sqlite.db.SupportSQLiteQuery
import evans18.portfolio.Constants.Database.TABLE_USERS
import evans18.portfolio.model.User

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: User)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(users: List<User>)

    @Update
    suspend fun update(user: User)

    @Delete
    suspend fun delete(user: User)

    @Query("SELECT * FROM $TABLE_USERS")
    suspend fun getAllUsers(): List<User>

    @Query("SELECT * FROM $TABLE_USERS WHERE id = :id")
    suspend fun findUserById(id: Int): User?

    @Query("SELECT * FROM $TABLE_USERS WHERE email = :email")
    suspend fun findUserByEmail(email: String): User?

    @RawQuery
    suspend fun findUserByQuery(query: SupportSQLiteQuery): User?

    @Query("DELETE FROM $TABLE_USERS WHERE email = :email;")
    suspend fun deleteByEmail(email: String)

    @Query("SELECT city FROM $TABLE_USERS WHERE id = :id")
    suspend fun getCityOfUserId(id: Int): String


}
package evans18.portfolio.repository

import android.util.Log
import evans18.portfolio.database.UserDao
import evans18.portfolio.model.User
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.rx2.asCoroutineDispatcher
import javax.inject.Inject

//todo: Need more practise with coroutines to figure out a better solution
class UserDataSource @Inject constructor(private val userDao: UserDao) : Repository<User> {
    private val coroutineDispatcher = Schedulers.single().asCoroutineDispatcher()

    override fun addItem(item: User) {
        GlobalScope.launch(coroutineDispatcher) {
            userDao.insert(item)
        }
    }

    override fun addItems(items: List<User>) {
        GlobalScope.launch(coroutineDispatcher) {
            userDao.insertAll(items)
        }
    }

    override fun getAll(): List<User> {
        return runBlocking(coroutineDispatcher) {
            userDao.getAllUsers()
        }
    }

    override fun getById(id: Int): User? {
        return runBlocking(coroutineDispatcher) {
            userDao.findUserById(id)
        }
    }

    override fun getByEmail(email: String): User? {
        return runBlocking(coroutineDispatcher) {
            userDao.findUserByEmail(email).also {
                Log.e(this::class.java.simpleName, "Loaded from Database:\n$it")
            }
        }
    }

    override fun deleteByEmail(email: String) {
        GlobalScope.launch(coroutineDispatcher) {
            userDao.deleteByEmail(email)
        }
    }

    override fun getCityOfUserWithEmail(email: String): String? {
        return runBlocking(coroutineDispatcher) {
            userDao.findUserByEmail(email)

            //todo: do this better
            userDao.findUserByEmail(email)?.let { return@runBlocking userDao.getCityOfUserId(it.id) }
            null
        }
    }
}
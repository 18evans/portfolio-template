package evans18.portfolio

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import evans18.portfolio.Constants.Owner.dummyOwner
import evans18.portfolio.Constants.Owner.ownerEmail
import evans18.portfolio.daggerentities.AppModule
import evans18.portfolio.daggerentities.DaggerAppComponent
import evans18.portfolio.daggerentities.DatabaseModule
import evans18.portfolio.maps.LocationActivity
import evans18.portfolio.repository.UserDataSource
import kotlinx.android.synthetic.main.motion_profile_appbar.*
import org.jetbrains.anko.longToast
import javax.inject.Inject

class ProfileActivity : AppCompatActivity() {

    @Inject
    lateinit var userDataSource: UserDataSource

    private val owner by lazy {
        userDataSource.getByEmail(ownerEmail).also {
            longToast("Found in DB user: $it")
        } ?: dummyOwner
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.motion_profile)
        DaggerAppComponent.builder()
            .appModule(AppModule(application))
            .databaseModule(DatabaseModule(application))
            .build()
            .inject(this)
    }

    override fun onResume() {
        super.onResume()
        //setup lazy property of owner here
        name.text = owner.name
        job.text = owner.job

    }

    fun callOwner(v: View?) {
        val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:${owner.phone}"))
        startActivity(intent)
    }

    fun sendEmailOwner(v: View?) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("mailto:${owner.email}"))
        intent.putExtra(Intent.EXTRA_SUBJECT, "Support request - ${getString(R.string.app_name)}")
        startActivity(intent)
    }

    fun openMyLocation(v: View?) {
        val intent = Intent(this, LocationActivity::class.java)
        startActivity(intent)
    }

}

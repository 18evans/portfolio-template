package evans18.portfolio.database

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import evans18.portfolio.model.User
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException


@RunWith(AndroidJUnit4::class)
@LargeTest
class UserDaoTest {

    private val userName = "name"
    private val userEmail = "test@email.com"
    private val userPhoneNr = "+31 6 2323 2323"
    private val job = "software dev"

    private lateinit var db: AppDatabase
    private lateinit var userDao: UserDao

// todo: rule gives AndroidJUnit4 class not loaded/found. why?
//    @Rule
//    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        val context: Context = ApplicationProvider.getApplicationContext()
//     db = AppDatabase.invoke(context)
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        )
            .allowMainThreadQueries()
            .build()
        userDao = db.userDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun insertUserTest() {
        runBlocking {
            val user = User(userName, userEmail)
            userDao.insert(user)

            val foundUser =
                userDao.findUserByEmail(userEmail)!!.copy(id = user.id) //change only id to please equals()

            assertEquals(user, foundUser)
            assertNotSame(user, foundUser)
        }
    }

    @Test
    fun updateUserTest() {
        runBlocking {
            userDao.insert(User(userName, userEmail))

            val originalUser = userDao.findUserByEmail(userEmail)

            val changedUser = originalUser!!.copy("newName", "newEmail")
            userDao.update(changedUser) //send update request in db

            assertNull(userDao.findUserByEmail(originalUser.email))

            val foundChangedUser = userDao
                .findUserByEmail(changedUser.email)

            assertNotEquals(originalUser, foundChangedUser)
            assertEquals(changedUser.copy(id = changedUser.id), foundChangedUser)
        }
    }

    @Test
    fun deleteUserTest() {
        runBlocking {
            userDao.insert(User(userName, userEmail))
            val originalUser = userDao.findUserByEmail(userEmail)

            userDao.delete(originalUser!!)
            assertNull(userDao.findUserByEmail(userEmail))
        }
    }
}
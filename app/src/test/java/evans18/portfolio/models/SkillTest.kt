package evans18.portfolio.models

import evans18.portfolio.model.Skill
import org.hamcrest.Matchers
import org.junit.Assert.*
import org.junit.Test


class SkillTest {

    private val name = "Java"
    private val type = Skill.SkillType.TECHNICAL
    private val experienceLevel = Skill.ExperienceLevel.MEDIOR
    private val skill = Skill(name, type, experienceLevel) //sut
    @Test
    fun shouldGetProperties() {
        assertEquals(name, skill.name)
        assertEquals(type, skill.type)
        assertEquals(experienceLevel, skill.experienceLevel)
    }

    @Test
    fun compareToOfSkillWithLowerExperienceLevelShouldReturnNegative() {
        val junior = Skill(
            "",
            Skill.SkillType.TECHNICAL,
            Skill.ExperienceLevel.JUNIOR
        )
        val senior = Skill(
            "",
            Skill.SkillType.TECHNICAL,
            Skill.ExperienceLevel.SENIOR
        )

        assertThat(junior.compareTo(senior), Matchers.lessThan(0))
    }

    @Test
    fun compareToOfSkillWithHigherExperienceLevelShouldReturnPositive() {
        val junior = Skill(
            "",
            Skill.SkillType.TECHNICAL,
            Skill.ExperienceLevel.JUNIOR
        )
        val senior = Skill(
            "",
            Skill.SkillType.TECHNICAL,
            Skill.ExperienceLevel.SENIOR
        )

        assertThat(senior.compareTo(junior), Matchers.greaterThan(0))
    }

    @Test
    fun compareToOfSkillsWithSameExperienceLevelReturnsZero() {
        val senior1 = Skill(
            "",
            Skill.SkillType.TECHNICAL,
            Skill.ExperienceLevel.SENIOR
        )
        val senior2 = Skill(
            "",
            Skill.SkillType.TECHNICAL,
            Skill.ExperienceLevel.SENIOR
        )

        assertEquals(0, senior1.compareTo(senior2))
    }

    @Test
    fun compareToJuniorIsLessThanMediorWhichIsLessThanSenior() {
        val junior = Skill(
            "",
            Skill.SkillType.TECHNICAL,
            Skill.ExperienceLevel.JUNIOR
        )
        val medior = Skill(
            "",
            Skill.SkillType.TECHNICAL,
            Skill.ExperienceLevel.MEDIOR
        )
        val senior = Skill(
            "",
            Skill.SkillType.TECHNICAL,
            Skill.ExperienceLevel.SENIOR
        )

        assertThat(junior.compareTo(medior), Matchers.lessThan(0))
        assertThat(medior.compareTo(senior), Matchers.lessThan(0))
    }


    @Test
    fun skillWithHigherExperienceShouldBeSortedFirstInACollection() {
        val originalArray = listOf(
            Skill(
                "skill1",
                Skill.SkillType.TECHNICAL,
                Skill.ExperienceLevel.JUNIOR
            ),
            Skill(
                "skill2",
                Skill.SkillType.TECHNICAL,
                Skill.ExperienceLevel.MEDIOR
            ),
            Skill(
                "skill3",
                Skill.SkillType.TECHNICAL,
                Skill.ExperienceLevel.SENIOR
            )
        )
        val arrayToSort = mutableListOf<Skill>()

        originalArray.forEach { arrayToSort.add(it.copy()) }

        arrayToSort.sort()

        for (i in 0..originalArray.size - 1) {
            println(i)
            assertEquals(
                "All property values within the objects were expected to be equal, but are NOT!",
                originalArray[i],
                arrayToSort[i]
            )
            assertNotSame(
                "Object were NOT expected to be pointing to the same reference, but are!",
                originalArray[i],
                arrayToSort[i]
            )
        }

    }
}
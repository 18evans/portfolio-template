package evans18.portfolio.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import evans18.portfolio.Constants.Database.TABLE_USERS

@Entity(tableName = TABLE_USERS)
data class User(
//    @Embedded
    val name: String,
    val email: String,
    val phone: String = "",
    val job: String = "", //todo: try @Embedded LatLng
    val city: String = "",
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0
)




package evans18.portfolio

import evans18.portfolio.model.User

object Constants {
    object Owner {
        const val ownerName = "Alex Evans"
        const val ownerEmail = "appowner3000@hottestmail.com"
        const val ownerPhone = "+31 6 6666 6666"
        const val ownerJob = "Software Developer"
        const val ownerCity = "Eindhoven"
        const val ownerLat = 51.4434
        const val ownerLong = 5.4792
        val dummyOwner = User(ownerName, ownerEmail, ownerPhone, ownerJob, ownerCity)
    }

    object Database {
        const val DB_NAME = "portfolio-database"
        const val TABLE_USERS = "users"
        const val DB_VERSION = 1
    }
}
package evans18.portfolio.daggerentities

import android.app.Application
import android.util.Log
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import dagger.Module
import dagger.Provides
import evans18.portfolio.Constants.Database.DB_NAME
import evans18.portfolio.Constants.Owner.ownerCity
import evans18.portfolio.Constants.Owner.ownerEmail
import evans18.portfolio.Constants.Owner.ownerJob
import evans18.portfolio.Constants.Owner.ownerName
import evans18.portfolio.Constants.Owner.ownerPhone
import evans18.portfolio.database.AppDatabase
import evans18.portfolio.database.UserDao
import evans18.portfolio.model.User
import evans18.portfolio.repository.UserDataSource
import javax.inject.Singleton


@Module
class DatabaseModule(private val application: Application) {

    private val db: AppDatabase =
        Room.databaseBuilder(application, AppDatabase::class.java, DB_NAME)
            .addCallback(object : RoomDatabase.Callback() {
                override fun onCreate(d: SupportSQLiteDatabase) {
                    super.onCreate(d)
                    //prepopulate with owner
                    val owner = User(ownerName, ownerEmail, ownerPhone, ownerJob, ownerCity)
                    getUserDataSource(getUserDao(getDatabase())).addItem(owner)
                    Log.e(this::class.java.simpleName, "Owner inserted: $owner")
                }
            }).build()

    @Singleton
    @Provides
    fun getDatabase(): AppDatabase {
        return db
    }

    @Singleton
    @Provides
    fun getUserDao(demoDatabase: AppDatabase): UserDao {
        return demoDatabase.userDao()
    }

    @Singleton
    @Provides
    fun getUserDataSource(userDao: UserDao): UserDataSource {
        return UserDataSource(userDao)
    }

}
package evans18.portfolio.repository

import evans18.portfolio.model.User


interface Repository<T> {
    fun addItem(item: T)
    fun addItems(items: List<T>)
    fun getAll(): List<User>
    fun getById(id: Int): T?
    fun getByEmail(email: String): T?
    fun deleteByEmail(email: String)
    fun getCityOfUserWithEmail(email: String): String?

    //todo    fun insertAll()
}

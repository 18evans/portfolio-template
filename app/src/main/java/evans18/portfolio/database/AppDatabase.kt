package evans18.portfolio.database

import androidx.room.Database
import androidx.room.RoomDatabase
import evans18.portfolio.Constants.Database.DB_VERSION
import evans18.portfolio.model.User

@Database(entities = arrayOf(User::class), version = DB_VERSION)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
}